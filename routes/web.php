<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::resource('/artworks', \App\Http\Controllers\ArtworkController::class);
