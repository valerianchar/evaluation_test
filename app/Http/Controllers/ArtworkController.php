<?php

namespace App\Http\Controllers;

use App\Http\Request\StoreArtworkRequest;
use App\Http\Request\UpdateArtworkRequest;
use App\Models\Artwork;
use Illuminate\Http\Request;
use Inertia\Inertia;

class ArtworkController extends Controller
{
    public function index()
    {
        $artworks = Artwork::all();
        return Inertia::render('Artworks/Index', ['artworks' => $artworks]);
    }

    public function create()
    {
        return Inertia::render('Artworks/Create');
    }

    public function store(
        StoreArtworkRequest $request
    ): \Illuminate\Http\RedirectResponse
    {
        $validatedData = $request->validate([
            'title' => 'required|max:255',
            'artist' => 'required|max:255',
            'description' => 'required|max:255',
            'status' => 'max:255',
        ]);

        Artwork::create($validatedData);
        return redirect()->route('artworks.index');
    }

    public function show(
        Artwork $artwork
    ): \Inertia\Response
    {
        return Inertia::render('Artworks/Show', ['artwork' => $artwork]);
    }

    public function edit(
        Artwork $artwork
    ): \Inertia\Response
    {
        return Inertia::render('Artworks/Edit', ['artwork' => $artwork]);
    }

    public function update(
        UpdateArtworkRequest $request,
        Artwork $artwork
    ): \Illuminate\Http\RedirectResponse
    {
        $validatedData = $request->validate([
            'id' => 'required|max:255',
            'title' => 'required|max:255',
            'artist' => 'required|max:255',
            'description' => 'required|max:255',
            'status' => 'max:255',
        ]);

        $artwork->update($validatedData);
        return redirect()->route('artworks.index');
    }

    public function destroy(
        Artwork $artwork
    )
    {
        $artwork->delete();
        return redirect()->route('artworks.index');
    }
}
