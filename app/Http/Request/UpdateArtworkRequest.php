<?php

namespace App\Http\Request;

use Illuminate\Foundation\Http\FormRequest;

class UpdateArtworkRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'id' => ['required', 'string', 'max:255'],
            'description' => ['required', 'text', 'max:255'],
            'title' => ['required', 'string', 'max:255'],
            'status' => ['string', 'max:255'],
            'artist' => ['required', 'string', 'max:255'],
        ];
    }
}
