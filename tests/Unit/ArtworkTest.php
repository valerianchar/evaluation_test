<?php

namespace Tests\Unit;

use Tests\TestCase;  // Importez TestCase de Laravel
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\Artwork;


class ArtworkTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function an_artwork_can_be_created()
    {
        $artwork = Artwork::factory()->create([
            'title' => 'La Nuit étoilée',
            'artist' => 'Vincent Van Gogh',
            'description' => 'Une des peintures les plus reconnues dans l\'histoire de la culture occidentale.'
        ]);

        $this->assertModelExists($artwork);
        $this->assertEquals('La Nuit étoilée', $artwork->title);
        $this->assertEquals('Vincent Van Gogh', $artwork->artist);
        $this->assertEquals('Une des peintures les plus reconnues dans l\'histoire de la culture occidentale.', $artwork->description);
    }

    /** @test */
    public function title_is_required()
    {
        $this->expectException(\Illuminate\Database\QueryException::class);
        Artwork::factory()->create(['title' => null]);
    }

    /** @test */
    public function artist_is_required()
    {
        $this->expectException(\Illuminate\Database\QueryException::class);
        Artwork::factory()->create(['artist' => null]);
    }

    /** @test */
    public function description_is_nullable()
    {
        $artwork = Artwork::factory()->create(['description' => null]);
        $this->assertNull($artwork->description);
    }

    /** @test */
    public function status_defaults_to_active()
    {
        $artwork = Artwork::factory()->create(['status' => null]);
        $this->assertEquals('active', $artwork->status);
    }

    /** @test */
    public function it_can_change_status_to_inactive()
    {
        $artwork = Artwork::factory()->create(['status' => 'inactive']);
        $this->assertEquals('inactive', $artwork->status);
    }

    /** @test */
    public function it_can_retrieve_active_artworks_using_a_scope()
    {
        Artwork::factory()->count(3)->create(['status' => 'active']);
        Artwork::factory()->count(2)->create(['status' => 'inactive']);

        $activeArtworks = Artwork::where('status', 'active')->get();
        $inactiveArtworks = Artwork::where('status', 'inactive')->get();

        $this->assertCount(3, $activeArtworks);
        $this->assertCount(2, $inactiveArtworks);
    }

    /** @test */
    public function it_can_update_its_attributes()
    {
        $artwork = Artwork::factory()->create(['title' => 'Original Title']);
        $artwork->update(['title' => 'Updated Title']);

        $this->assertEquals('Updated Title', $artwork->title);
    }

    /** @test */
    public function it_can_be_deleted()
    {
        $artwork = Artwork::factory()->create();
        $artworkId = $artwork->id;
        $artwork->delete();
        $this->assertDatabaseMissing('artworks', ['id' => $artworkId]);
    }
}
