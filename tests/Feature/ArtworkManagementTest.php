<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Artwork;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;

class ArtworkManagementTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /** @test */
    public function a_list_of_artworks_can_be_retrieved()
    {
        $artworks = Artwork::factory()->count(3)->create();

        $response = $this->get(route('artworks.index'));

        $response->assertStatus(200);
        $response->assertInertia(fn ($page) => $page
            ->component('Artworks/Index')
            ->has('artworks', 3)
        );
    }

    /** @test */
    public function the_artwork_create_form_can_be_displayed()
    {
        $response = $this->get(route('artworks.create'));

        $response->assertStatus(200);
        $response->assertInertia(fn ($page) => $page
            ->component('Artworks/Create')
        );
    }

    /** @test */
    public function an_artwork_can_be_stored()
    {
        $formData = [
            'title' => 'New Artwork',
            'artist' => 'New Artist',
            'description' => 'New Description',
            'status' => 'active'
        ];

        $response = $this->post(route('artworks.store'), $formData);

        $response->assertRedirect(route('artworks.index'));
        $this->assertDatabaseHas('artworks', $formData);
    }

    /** @test */
    public function an_artwork_can_be_displayed()
    {
        $artwork = Artwork::factory()->create();

        $response = $this->get(route('artworks.show', $artwork));

        $response->assertStatus(200);
        $response->assertInertia(fn ($page) => $page
            ->component('Artworks/Show')
            ->has('artwork', fn ($page) => $page
                ->where('id', $artwork->id)
                ->etc()
            )
        );
    }

    /** @test */
    public function the_artwork_edit_form_can_be_displayed()
    {
        $artwork = Artwork::factory()->create();

        $response = $this->get(route('artworks.edit', $artwork));

        $response->assertStatus(200);
        $response->assertInertia(fn ($page) => $page
            ->component('Artworks/Edit')
            ->has('artwork', fn ($page) => $page
                ->where('id', $artwork->id)
                ->etc()
            )
        );
    }

    /** @test */
    public function an_artwork_can_be_updated()
    {
        $artwork = Artwork::factory()->create();

        $formData = [
            'title' => 'Updated Title',
            'artist' => 'Updated Artist',
            'description' => 'Updated Description',
            'status' => 'inactive'
        ];

        $response = $this->put(route('artworks.update', $artwork), $formData);

        $response->assertRedirect(route('artworks.index'));
        $this->assertDatabaseHas('artworks', array_merge(['id' => $artwork->id], $formData));
    }

    /** @test */
    public function an_artwork_can_be_deleted()
    {
        $artwork = Artwork::factory()->create();

        $response = $this->delete(route('artworks.destroy', $artwork));

        $response->assertRedirect(route('artworks.index'));
        $this->assertSoftDeleted('artworks', ['id' => $artwork->id]);
    }

}
